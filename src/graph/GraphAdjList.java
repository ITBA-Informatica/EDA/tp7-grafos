package graph;

import com.sun.javaws.exceptions.InvalidArgumentException;

import java.util.*;

/**
 * 
 * Clase abstracta para Grafos (no multigrafos). No soporta multigrafos ni lazos
 * 
 * @param <V> informacion en los nodos
 * @param <E> informacion en los ejes (solo para informar, no sirve para caminos minimos ya que no se establece un peso
 */
public abstract class GraphAdjList<V, E> {

	/* Definimos una clase interna para los nodos. En cada uno se almacenara su informacion y la lista de adyacentes */
	private class Node {
		public V info;
		public boolean visited;
		public int color;
		public List<Arc> adj; // en cada arco se almacena info y nodo vecino */

		public Node(V info) {
			this.info = info;
			this.visited = false;
			this.adj = new ArrayList<Arc>();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((info == null) ? 0 : info.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final Node other = (Node) obj;
			if (info == null) {
				if (other.info != null)
					return false;
			} else if (!info.equals(other.info))
				return false;
			return true;
		}
	}

	

	private class Arc {
		public E info;
		public Node neighbor;

		public Arc(E info, Node neighbor) {
			super();
			this.info = info;
			this.neighbor = neighbor;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((neighbor == null) ? 0 : neighbor.hashCode());
			return result;
		}

		// Consideramos que son iguales si "apuntan" al mismo nodo (para no agregar
		// dos ejes al mismo nodo)
		// La UNICA justificacion para implementar equals de esta forma es por el uso que se da en addArc y removeArc, al verificar si ya existe un arco entre dos nodos (ver comentario en esos metodos).
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final Arc other = (Arc) obj;
			if (neighbor == null) {
				if (other.neighbor != null)
					return false;
			} else if (!neighbor.equals(other.neighbor))
				return false;
			return true;
		}

	}

	/*
	** Debemos tener la coleccion de nodos. Podria ser una lista, un conjunto, etc.
	** Se decidio por un mapa porque la operacion mas comun es ubicar un nodo en base a la informacion que suministra el usuario.
	*/
	private HashMap<V, Node> nodes;	
	// Seria mejor que la definicion se haga con la interface, o sea
	// 	private Map<V, Node> nodes;	

	/*
	** En vez de recibir en el constructor un boolean para determinar si es dirigido o no
	** se distingue con un metodo abstracto. Pero el comportamiento sigue establecido por completo en esta clase, 
	** por lo que sigue siendo una mala implementacion: el comportamiento deberia
	** estar implementado en cada clase concreta
	*/
	protected abstract boolean isDirected();

	public GraphAdjList() {
		this.nodes = new HashMap<V, Node>();
	}

	public boolean isEmpty() {
		return nodes.size() == 0;
	}


	public void addVertex(V vertex) {
		if (!nodes.containsKey(vertex)) {
			nodes.put(vertex, new Node(vertex));
		}
	}




	public void addArc(V v, V w, E e) {
		Node origin = nodes.get(v);
		Node dest = nodes.get(w);
		if (origin != null && dest != null && !origin.equals(dest)) { 
			// Agregar un arco deberia ser un metodo de la clase Node
			Arc arc = new Arc(e, dest);
			// Dependemos de la implementacion de Arc.equals(), no es
			// seguro el uso de contains.
			if (!origin.adj.contains(arc)) {
				origin.adj.add(arc);
				if (!isDirected()) {
					dest.adj.add(new Arc(e, origin));
				}
			}
		}
	}

	public int arcCount() {
		int count = 0;
		for (Node n : getNodes())
			count += n.adj.size();
		if (!isDirected())
			count /= 2;
		return count;
	}


	public void RemoveArc(V v, V w) {
		Node origin = nodes.get(v);
		if (origin == null)
			return;
		Node dest = nodes.get(w);
		if (dest == null)
			return;
		// No es seguro el remove ya que depende de Arc.equals.
		// La eliminacion del arco deberia ser un metodo de Node
		origin.adj.remove(new Arc(null, dest)); 
		if (!isDirected())
			dest.adj.remove(new Arc(null, origin)); 
	}


	public E isArc(V v, V w) {
		Node origin = nodes.get(v);
		if (origin == null)
			return null;
		
		// En este caso no dependemos de Arc.equals, el metodo es mas seguro
		// Pero... deberia ser un metodo de Node
		for (Arc e : origin.adj) {
			if (e.neighbor.info.equals(w)) {
				return e.info;
			}
		}
		return null;

	}

	public int outDegree(V v) {
		Node node = nodes.get(v);
		if (node != null) {
			return node.adj.size();
		}
		return 0;
	}


	public int inDegree(V v) {
		if (!isDirected())
			return outDegree(v);		// Sobran los comentarios...
		int count = 0;
		Node node = nodes.get(v);
		for (Node n : getNodes()) { // Recorremos lista de nodos
			if (!n.equals(node)) {
				for (Arc adj : n.adj)
					// Recorremos lista de adyacencia
					if (adj.neighbor.equals(node))
						count++;
			}
		}
		return count;
	}


	public List<V> neighbors(V v) {
		Node node = nodes.get(v);
		if (node == null) 
			return null;

		List<V> l = new ArrayList<V>(node.adj.size());
		for (Arc e : node.adj) {
			l.add(e.neighbor.info);
		}
		return l;
	}


	public void removeVertex(V v) {
		Node node = nodes.get(v);
		if (node == null)
			return;

		// Primero removerlo de la lista de adyacencia de sus vecinos
		Arc e = new Arc(null, node);	// arco "ficticio" para poder eliminar el vecino
		for (Node n : getNodes()) {
			// no es seguro el uso de remove ya que depende de Arc.equals
			// la eliminacion del vecino podria estar en la clase Node
			if (!n.equals(node))
				n.adj.remove(e);
		}

		// Eliminar el nodo
		nodes.remove(v);
	}

	public int vertexCount() {
		return nodes.size();
	}

	/*
	** Metodo auxiliar para iterar sobre los nodos
	*/
	private List<Node> getNodes() {
		List<Node> l = new ArrayList<Node>(vertexCount());
		Iterator<V> it = nodes.keySet().iterator();
		while (it.hasNext()) {
			l.add(nodes.get(it.next()));
		}
		return l;
	}

	public List<V> iterativeDFS(V origin) {
	    Node node = nodes.get(origin);

	    if (node == null) {
	        return null;
        }
        clearMarks();

	    List<V> l = new ArrayList<>();
        Deque<Node> toVisit = new LinkedList<>();
        node.visited = true;
        toVisit.push(node);

        while(!toVisit.isEmpty()) {
            Node next = toVisit.pop();
            l.add(next.info);

            for (Arc e : next.adj) {
                if (!e.neighbor.visited) {
                    e.neighbor.visited = true;
                    toVisit.push(e.neighbor);
                }
            }
        }
	    return l;
    }

    public Graph<V,E> minHeightTree(V origin) {
        Node node = nodes.get(origin);
        if (node == null)
            return null;
        clearMarks();

        Graph<V,E> tree = new Graph<>();
        tree.addVertex(node.info);
        node.visited = true;
        Queue<Node> q = new LinkedList<>();
        q.offer(node);
        while(!q.isEmpty()) {
            Node next = q.poll();
            for (Arc e : next.adj) {
                Node en = e.neighbor;
                if (!en.visited) {
                    q.offer(en);
                    en.visited = true;
                    tree.addVertex(en.info);
                    tree.addArc(next.info, en.info, e.info);
                }
            }
        }
        return tree;
    }

	public List<V> DFS(V origin) {
		Node node = nodes.get(origin);
		if (node == null)
			return null;
		clearMarks();
		List<V> l = new ArrayList<V>();
		this.DFS(node, l);
		return l;
	}

	public Boolean isTree() {
	    if (isEmpty()) return true;
	    clearMarks();
	    Node first = nodes.entrySet().iterator().next().getValue();
	    return isTree(first,null);
    }

    protected Boolean isTree(Node curr, Node parent) {
	    curr.visited = true;
	    for (Arc e : curr.adj) {
	        Node en = e.neighbor;
	        if (en != parent) {
                if (en.visited || !isTree(en,curr)) {
                        return false;
                }
	        }
        }
        return true;
    }

    public int cantCaminos(V origen, V destino) {
	    clearMarks();
        Node o = nodes.get(origen);
        Node d = nodes.get(destino);
        if (o == null || d == null) {
            throw new IllegalArgumentException("Nodes not found");
        }
        return cantCaminos(o,d, "[");
    }

    protected int cantCaminos(Node o, Node d, String chain) {
	    if (o == d) {
            chain += ", " + o.info + "]";
            System.out.println(chain);
            return 1;
        }
        o.visited = true;
        int caminos = 0;
        if (chain == "[") {
            chain += o.info;
        } else {
            chain += ", " + o.info;
        }
	    for (Arc e : o.adj) {
	        if (!e.neighbor.visited) {
                caminos += cantCaminos(e.neighbor, d, chain );
            }
        }
        o.visited = false;
	    return caminos;
    }

    public boolean isNPartite(int n) {
	    clearMarks();
	    if (isEmpty()) return true;
	    if (n == 1) return true;
	    if (n <= 0 ) throw new IllegalArgumentException();
	    Iterator<Node> it = nodes.values().iterator();
	    while(it.hasNext()) {
	        Node next = it.next();
	        if (!next.visited) {
	            if(!isNPartite(next,0,n)) {
	                return false;
                }
            }
        }
        return true;
    }

    protected boolean isNPartite(Node n, int color, int maxColor) {
        n.visited = true;
        n.color = color;
        boolean r = true;
        for (Arc e : n.adj) {
            Node en = e.neighbor;
            if (en.visited && en.color == n.color) {
                return false;
            }
            if (!en.visited) {
                if(!isNPartite(en,(color + 1) % maxColor, maxColor)) {
                    return false;
                }
            }
        }
        return true;
    }



	protected void clearMarks() {
		for (Node n : getNodes()) {
			n.visited = false;
		}

	}


	protected void DFS(Node origin, List<V> l) {
		if (origin.visited)
			return;
		l.add(origin.info);
		origin.visited = true;
		for (Arc e : origin.adj){
			// Podria verificarse si e.neighbor ya fue visitado y evitar una llamada recursiva (eliminar el if del comienzo)
			DFS(e.neighbor, l);
		}
	}


	public List<V> BFS(V origin) {
		Node node = nodes.get(origin);
		if (node == null)
			return null;
		clearMarks();
		List<V> l = new ArrayList<V>();

		Queue<Node> q = new LinkedList<Node>();
		q.add(node);
		node.visited = true;
		while (!q.isEmpty()) {
			node = q.poll();
			l.add(node.info);
			for (Arc e : node.adj) {
				if (!e.neighbor.visited) {
					e.neighbor.visited = true;	// Para evitar volver a encolarlo
					q.add(e.neighbor);
				}
			}
		}
		return l;
	}

	public boolean isConnected() {
		if (isEmpty()) {
			return true;
		}
		clearMarks();
		List<Node> l = getNodes();
		List<V> laux = new ArrayList<V>();
		DFS(l.get(0), laux);
		for (Node node : l) {
			if (!node.visited) {
				return false;
			}
		}
		return true;
	}

	public int connectedComponents() {
		clearMarks();
		int count = 0;
		Node node;
		while ((node = unvisited()) != null) {
			count++;
			DFS(node, new ArrayList<V>());
		}
		return count;
	}

	private Node unvisited() {
		for(Node node : getNodes()) {
			if (! node.visited )
				return node;
		}
		return null;
	}

}
